package cn.xlbweb.admincloud.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description: TODO Feign组件的超时和重试
 */
@FeignClient(name = "admincloud-auth", fallback = AuthClientFallBack.class)
public interface AuthClient {

    @GetMapping("/demo/hello")
    String hello(@RequestParam(value = "name") String name, @RequestParam(value = "age") Integer age);
}
