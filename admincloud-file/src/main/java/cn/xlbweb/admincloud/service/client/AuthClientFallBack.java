package cn.xlbweb.admincloud.service.client;

import org.springframework.stereotype.Component;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description:
 */
@Component
public class AuthClientFallBack implements AuthClient {

    @Override
    public String hello(String name, Integer age) {
        return "调用授权服务hello接口失败";
    }
}
