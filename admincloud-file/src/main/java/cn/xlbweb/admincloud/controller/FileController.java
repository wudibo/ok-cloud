package cn.xlbweb.admincloud.controller;

import cn.xlbweb.admincloud.service.client.AuthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description:
 */
@RestController
@RequestMapping("file")
public class FileController {

    @Autowired
    private AuthClient authClient;

    @GetMapping("hello")
    public String index() {
        return authClient.hello("小明", 20);
    }

    @GetMapping("list")
    public Map<String, String> list() {
        System.out.println("...test...");
        Map<String, String> map = new HashMap<>(8);
        map.put("pic1", "https://www.baidu.com/img/bd_logo1.png");
        map.put("pic2", "https://www.baidu.com/img/bd_logo1.png");
        map.put("pic3", "https://www.baidu.com/img/bd_logo1.png");
        return map;
    }
}
