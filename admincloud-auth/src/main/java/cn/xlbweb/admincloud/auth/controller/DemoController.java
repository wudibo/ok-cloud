package cn.xlbweb.admincloud.auth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description:
 */
@RestController
@RequestMapping("demo")
public class DemoController {

    @GetMapping
    public String index() {
        return "auth demo index";
    }

    @GetMapping("hello")
    public String hello(String name, Integer age) {
        System.out.println("name=" + name);
        System.out.println("age=" + age);
        return "auth demo hello";
    }
}
