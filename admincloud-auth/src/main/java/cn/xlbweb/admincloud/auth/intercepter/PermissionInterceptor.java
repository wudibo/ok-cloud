package cn.xlbweb.admincloud.auth.intercepter;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: bobi
 * @date: 2019-06-23 21:42
 * @description:
 */
@Slf4j
public class PermissionInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String userId = request.getHeader("x-user-id");
        // TODO 根据userID查询数据库获取该用户的所有的菜单url+角色
        String uri = request.getRequestURI();
        System.out.println(uri);
        if (userId == "666666" && uri == "/file/test") {
            log.info("userId={}拥有{}权限", userId, "/file/test");
            return true;
        }
        log.warn("userId={}没有{}权限", userId, "/file/test");
        return false;
    }
}
