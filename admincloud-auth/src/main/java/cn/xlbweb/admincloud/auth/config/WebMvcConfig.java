package cn.xlbweb.admincloud.auth.config;

import cn.xlbweb.admincloud.auth.intercepter.PermissionInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: bobi
 * @date: 2019-06-23 21:42
 * @description:
 */
@Configuration
@Slf4j
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info("init PermissionInterceptor...");
        registry.addInterceptor(new PermissionInterceptor());
    }
}
