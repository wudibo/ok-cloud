package cn.xlbweb.admincloud.auth.pojo.dataobject;

import lombok.Data;

import java.util.Date;

/**
 * @author: bobi
 * @date: 2019-06-23 21:42
 * @description:
 */
@Data
public class UserDO {

    private Integer id;
    private String username;
    private String password;
    private String email;
    private Date createTime;
    private Date updateTime;
}
