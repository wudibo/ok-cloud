package cn.xlbweb.admincloud.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description: App
 * http://localhost:8081/hystrix
 */
@SpringCloudApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
