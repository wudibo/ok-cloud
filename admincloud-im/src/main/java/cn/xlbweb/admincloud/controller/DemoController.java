package cn.xlbweb.admincloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description:
 */
@RestController
@RequestMapping("demo")
public class DemoController {

    @GetMapping
    public String index() {
        return "im demo";
    }
}
