package cn.xlbweb.admincloud;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description:
 */
@SpringCloudApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
