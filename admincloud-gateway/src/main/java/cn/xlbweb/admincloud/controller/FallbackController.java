package cn.xlbweb.admincloud.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description:
 */
@RestController
public class FallbackController {

    @GetMapping("fallback")
    public String fallback() {
        return "Spring Cloud Gateway fallback!";
    }
}
