package cn.xlbweb.admincloud.controller;

import cn.xlbweb.admincloud.util.JwtUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: bobi
 * @date: 2019-03-16 15:00
 * @description:
 */
@RestController
public class DemoController {

    @GetMapping("demo")
    public String index() {
        return "gateway demo index";
    }

    @GetMapping("getToken")
    public String getToken(String userId) {
        return JwtUtils.generateToken(userId);
    }
}
