package cn.xlbweb.admincloud.util;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author: bobi
 * @date: 2019-06-30 21:21
 * @description:
 */
public class Java8DateTest {

    @Test
    public void test01() {
        // 当前日期
        LocalDate nowDate = LocalDate.now();
        System.out.println(nowDate);

        // 当前时间
        LocalTime nowTime = LocalTime.now();
        System.out.println(nowTime);

        // 当前日期时间
        LocalDateTime nowDateTime = LocalDateTime.now();
        System.out.println(nowDateTime);

        // 当前日期时间+1H
        LocalDateTime plusDateTime = nowDateTime.plus(1, ChronoUnit.HOURS);
        System.out.println(plusDateTime);

        // 格式化时间
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDateTime = plusDateTime.format(formatter);
        System.out.println(formatDateTime);

        // LocalDateTime -> Date
        ZonedDateTime zonedDateTime = plusDateTime.atZone(ZoneId.systemDefault());
        Date date = Date.from(zonedDateTime.toInstant());
        System.out.println(date);

        // Date -> LocalDateTime
        LocalDateTime localDateTime = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        System.out.println(localDateTime);
    }
}
