package cn.xlbweb.admincloud.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtilsTest {

    private final String secretKey = "abcdefghijklmnopqrstuvwxyz";

    @Test
    public void test01() {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", "admin");
        LocalDateTime plusDateTime = LocalDateTime.now().plus(1, ChronoUnit.HOURS);
        Date expirationDate = Date.from(plusDateTime.atZone(ZoneId.systemDefault()).toInstant());
        String token = Jwts.builder().setSubject("user_info").setClaims(map).signWith(SignatureAlgorithm.HS512, secretKey).setExpiration(expirationDate).compact();
        // eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1NjE0Mjg0MDQsInVzZXJJZCI6ImFkbWluIn0.ZJXI5mpJmOq8QrRei0iM7Zi-4JmCw9ZYxP-SbKMAh7j1agzE5SInMP7bUJk75LQD9WJ9EVVirFUvPLQsI_hzQg
        System.out.println(token);
    }

    @Test
    public void test02() {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1NjE0Mjg0MDQsInVzZXJJZCI6ImFkbWluIn0.ZJXI5mpJmOq8QrRei0iM7Zi-4JmCw9ZYxP-SbKMAh7j1agzE5SInMP7bUJk75LQD9WJ9EVVirFUvPLQsI_hzQg";
        Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
        // 签名
        String signature = claims.getSignature();
        // header信息
        Map<String, Object> header = claims.getHeader();
        // body信息
        Map<String, Object> body = claims.getBody();
        // 主题信息
        String subject = claims.getBody().getSubject();
        // 过期时间
        Date expirationDate = claims.getBody().getExpiration();
        // 接受者
        String audience = claims.getBody().getAudience();
        // 签发者
        String issuer = claims.getBody().getIssuer();
        // 签发时间
        Date issuedAt = claims.getBody().getIssuedAt();
        // jwt id
        String id = claims.getBody().getId();

        System.out.println("header-->" + header);
        System.out.println("signature-->" + signature);
        System.out.println("body-->" + body);
        System.out.println("subject-->" + subject);
        System.out.println("expirationDate-->" + expirationDate);
        System.out.println("audience-->" + audience);
        System.out.println("issuer-->" + issuer);
        System.out.println("issuedAt-->" + issuedAt);
        System.out.println("id-->" + id);
    }



    @Test
    public void test04() {
        String token = JwtUtils.generateToken("666666");
        System.out.println(token);
        // eyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOiI2NjY2NjYifQ.482O6lnRYKQUl84I31LGQ2qTIthrjzhJYsrhkv5mmsLm520VbfxzwYE16urpbQGbiLtv346FvzNu38Yz6dWxTA
        String userId = JwtUtils.parseToken("Bearer xxeyJhbGciOiJIUzUxMiJ9.eyJ1c2VySWQiOiI2NjY2NjYifQ.482O6lnRYKQUl84I31LGQ2qTIthrjzhJYsrhkv5mmsLm520VbfxzwYE16urpbQGbiLtv346FvzNu38Yz6dWxTA\n");
        System.out.println(userId);
    }
}
